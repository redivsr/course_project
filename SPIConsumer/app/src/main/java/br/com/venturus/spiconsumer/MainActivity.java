package br.com.venturus.spiconsumer;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import br.com.venturus.spiconsumer.data.Position;
import br.com.venturus.spiconsumer.data.User;
import br.com.venturus.spiconsumer.views.Canvas;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private List<User> mUserList = new ArrayList<>();
    private Handler mHandler;
    private Runnable mRunnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;

        final Canvas canvas = findViewById(R.id.spi_consumer);
        int proportionWidth = (int) (width/2.16);
        int proportionHeight = (int) (height/3.84);
        RelativeLayout.LayoutParams layoutParams =
                new RelativeLayout.LayoutParams(proportionWidth * 2, proportionHeight * 2);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

        canvas.setLayoutParams(layoutParams);


        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = firebaseDatabase.getReference("macs");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mUserList.clear();
                if (mHandler != null && mRunnable != null) {
                    mHandler.removeCallbacks(mRunnable);
                    mHandler = null;
                    mRunnable = null;
                }

                List<Position> positions = new ArrayList<>();
                Iterator<DataSnapshot> macsIterator = dataSnapshot.getChildren().iterator();
                while (macsIterator.hasNext()) {
                    // This is all the macs that were found
                    DataSnapshot macData = macsIterator.next();
                    String mac = String.valueOf(macData.getKey());

                    Iterator<DataSnapshot> timestampsIterator = macData.getChildren().iterator();
                    while (timestampsIterator.hasNext()) {
                        // This is all the timestamps
                        DataSnapshot timestampData = timestampsIterator.next();
                        String timestamp = String.valueOf(timestampData.getKey());
                        String posX = "";
                        String posY = "";

                        Iterator<DataSnapshot> positionsIterator = timestampData.getChildren().iterator();
                        while (positionsIterator.hasNext()) {
                            DataSnapshot positionsData = positionsIterator.next();
                            if ("posx".equals(String.valueOf(positionsData.getKey()))) {
                                posX = String.valueOf(positionsData.getValue());
                            } else if ("posy".equals(String.valueOf(positionsData.getKey()))) {
                                posY = String.valueOf(positionsData.getValue());
                            }
                        }

                        positions.add(new Position(timestamp, posX, posY));
                    }

                    mUserList.add(new User(mac, positions, Color.BLUE));
                }

                for (int i = 0; i < getNumberOfBiggestPositions(mUserList); i++) {
                    final int index = i;
                    long delayAnimation = (i+1) * 5000/getNumberOfBiggestPositions(mUserList);
                    mHandler = new Handler();
                    mRunnable = new Runnable() {
                        @Override
                        public void run() {
                            canvas.setCurrentUsers(mUserList, index);
                        }
                    };

                    mHandler.postDelayed(mRunnable, delayAnimation);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private long getNumberOfBiggestPositions(List<User> mUserList) {
        long biggestPosition = 0;
        for (User user : mUserList) {
            if (user.getPositions().size() > biggestPosition) {
                biggestPosition = user.getPositions().size();
            }
        }

        return biggestPosition;
    }
}
