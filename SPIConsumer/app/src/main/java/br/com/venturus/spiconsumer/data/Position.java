package br.com.venturus.spiconsumer.data;

/**
 * Created by tiagodvl on 10/10/17.
 */

public class Position {

    private String timeStamp;
    private String posX;
    private String posY;

    public Position(String timeStamp, String posX, String posY) {
        this.timeStamp = timeStamp;
        this.posX = posX;
        this.posY = posY;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getPosX() {
        return posX;
    }

    public void setPosX(String posX) {
        this.posX = posX;
    }

    public String getPosY() {
        return posY;
    }

    public void setPosY(String posY) {
        this.posY = posY;
    }
}
