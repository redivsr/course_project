package br.com.venturus.spiconsumer.data;

import java.util.List;

/**
 * Created by tdavi on 21-Sep-17.
 */

public class User {

    private String mac;
    private List<Position> positions;
    private int color;

    public User(String mac, List<Position> positionList, int color) {
        this.mac = mac;
        this.positions = positionList;
        this.color = color;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
