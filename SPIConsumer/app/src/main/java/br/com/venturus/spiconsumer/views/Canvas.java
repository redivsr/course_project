package br.com.venturus.spiconsumer.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import br.com.venturus.spiconsumer.R;
import br.com.venturus.spiconsumer.data.Position;
import br.com.venturus.spiconsumer.data.User;

/**
 * Created by tdavi on 21-Sep-17.
 */

public class Canvas extends View {

    private static final String TAG = "Canvas";
    private static final int MCU_SQUARE_SIZE = 30;
    private static final int MCU_USER_CIRCLE_SIZE = 25;

    private int mWallColor;
    private String mPlaceName;
    private float mGrowth;
    private int mWidth;
    private int mHeight;

    private Paint mRectanglePaint;
    private Paint mPlaceNamePaint;
    private Paint mNodesMCUPaint;
    private Paint mUserPaint[];
    private Rect mRectangle;
    private Rect[] mNodesMCU;
    private List<User> mUsersList;
    private int mCurrentIndex;
    private Position position;

    public void setPlaceName (String placeName) {
        this.mPlaceName = placeName;
        invalidate();
        requestLayout();
    }

    public void setCurrentUsers(List<User> userList, int currentIndex) {
        mUserPaint = new Paint[userList.size()];
        mUsersList = userList;
        mCurrentIndex = currentIndex;

        for (int i = 0; i < userList.size(); i++) {
            User user = userList.get(i);
            mUserPaint[i] = new Paint();
            mUserPaint[i].setColor(user.getColor());
        }

        invalidate();
        requestLayout();
    }

    public Canvas(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.getTheme()
                .obtainStyledAttributes(attrs, R.styleable.Canvas, 0, 0);

        try {
            mWallColor = typedArray.getColor(R.styleable.Canvas_wallColor, Color.BLACK);
            mPlaceName = typedArray.getString(R.styleable.Canvas_placeName);
            mGrowth = typedArray.getDimension(R.styleable.Canvas_growth, 20F);
        } finally {
            typedArray.recycle();
        }

        init();
    }

    private void init() {
        mRectanglePaint = new Paint();
        mRectanglePaint.setColor(mWallColor);
        mRectanglePaint.setStyle(Paint.Style.STROKE);
        mRectanglePaint.setStrokeWidth(10);

        mPlaceNamePaint = new Paint();
        mPlaceNamePaint.setColor(Color.GRAY);
        mPlaceNamePaint.setTextSize(mGrowth);
        mPlaceNamePaint.setTextAlign(Paint.Align.CENTER);

        mNodesMCUPaint = new Paint();
        mNodesMCUPaint.setColor(Color.RED);

        mUsersList = new ArrayList<>();
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mWidth = w;
        mHeight = h;
        mRectangle = new Rect(0, 0, mWidth, mHeight);
        mNodesMCU = new Rect[4];
        for (int i = 0; i < mNodesMCU.length; i++) {
            switch (i) {
                // LEFT, TOP, RIGHT, BOTTOM
                case 0: mNodesMCU[i] = new Rect(0, 0, MCU_SQUARE_SIZE, MCU_SQUARE_SIZE); break;
                case 1: mNodesMCU[i] = new Rect(mWidth - MCU_SQUARE_SIZE, 0, mWidth, MCU_SQUARE_SIZE); break;
                case 2: mNodesMCU[i] = new Rect(0, mHeight - MCU_SQUARE_SIZE, MCU_SQUARE_SIZE, mHeight); break;
                case 3: mNodesMCU[i] = new Rect(mWidth - MCU_SQUARE_SIZE, mHeight - MCU_SQUARE_SIZE, mWidth, mHeight); break;

            }
        }
    }

    @Override
    protected void onDraw(final android.graphics.Canvas canvas) {
        canvas.drawRect(mRectangle, mRectanglePaint);
        canvas.drawText(mPlaceName, (canvas.getWidth() / 2), (canvas.getHeight() / 2), mPlaceNamePaint);

        // Inflate all MCU sniffers
        for (Rect nodeMCURect : mNodesMCU) {
            canvas.drawRect(nodeMCURect, mNodesMCUPaint);
        }

        // Inflate current position of Users being sniffed by node MCUs
        for (int i = 0; i < mUsersList.size(); i++) {
            final User user = mUsersList.get(i);
            final Position position = getPosition(user);
            final Paint userPain = mUserPaint[i];
            canvas.drawCircle(
                    Integer.valueOf(position.getPosX()) * 2,
                    Integer.valueOf(position.getPosY()) * 2,
                    MCU_USER_CIRCLE_SIZE,
                    userPain
            );

        }
    }

    public Position getPosition(User user) {
        Position position;

        if (user.getPositions().size() - 1 < mCurrentIndex) {
            position = user.getPositions().get(user.getPositions().size() - 1);
        } else {
            position = user.getPositions().get(mCurrentIndex);
        }
        return position;
    }
}
