#include "circularbuffer.h"

MacRecord *macRecords = NULL;
uint32_t time_zero;

struct MacRet *uCheckMac(uint8_t *mac) {
    struct MacRet *macRet = (struct MacRet *) malloc (sizeof (struct MacRet));
    macRet->curr = macRecords;
    macRet->prev = NULL;
    int i;
    int eq = 0;
    while (macRet->curr != NULL) {
        for (i = 0; i < 6; i++) {
            if (mac[i] != macRet->curr->mac[i]) break;
            eq ++;
        }
        if (eq == 6) {
            break;
        }
        eq = 0;

        macRet->prev = macRet->curr;
        macRet->curr = macRet->curr->next;
    }
    return macRet;
}

void vInsertRecord(WlanPacket wlanPacket) {
    struct MacRet *res = uCheckMac(wlanPacket.mac);
    MacRecord *prev = res->prev;
    MacRecord *curr = res->curr;
    RssiRecord *rssiRecord = (RssiRecord*) malloc (sizeof(RssiRecord));
    rssiRecord->rssi = wlanPacket.rssiRecord->rssi;
    rssiRecord->timestamp = wlanPacket.rssiRecord->timestamp;
    rssiRecord->next = NULL;
    if (curr != NULL) {
        if (curr->qtt >= 20) {
            RssiRecord *tmp = curr->rssiRecords;
            while (tmp->next->next != NULL) {
                tmp = tmp->next;
            }
            free(tmp->next);
            tmp->next = NULL;
            tmp->timestamp = tmp->timestamp - time_zero;
        } else {
            (curr->qtt)++;
        }
        rssiRecord->next = curr->rssiRecords;
        curr->rssiRecords = rssiRecord;
    } else {
        if (prev == NULL) {
            macRecords = (MacRecord*) malloc (sizeof(MacRecord));
            curr = macRecords;
            time_zero = rssiRecord->timestamp;
        } else {
            prev->next = (MacRecord*) malloc (sizeof(MacRecord));
            curr = prev->next;
        }
        curr->mac = (uint8_t*) malloc (sizeof(uint8_t) * 6);
        int i = 0;
        for (i = 0; i < 6; i++) curr->mac[i] = wlanPacket.mac[i];
        curr->qtt = 1;
        rssiRecord->timestamp = rssiRecord->timestamp - time_zero;
        curr->rssiRecords = rssiRecord;
        curr->next = NULL;
        
    }
    free(res);
    res = NULL;
}

void print() {
    MacRecord *tmp = macRecords;
    while(tmp != NULL) {
        RssiRecord *rssiTmp = tmp->rssiRecords;
        int i = 0;
        for (; i < 6; i++) printf("%02x:", tmp->mac[i]);
        printf(" %d\t", tmp->qtt);
        while (rssiTmp != NULL) {
            printf("%d|%d|", rssiTmp->rssi, rssiTmp->timestamp);
            rssiTmp = rssiTmp->next;
        }
        printf("\n");
        tmp = tmp->next;
    }
}

void freeAll () {
    MacRecord *tmp = macRecords;
    while (tmp != NULL) {
        RssiRecord *rssiTmp = tmp->rssiRecords;
        while (rssiTmp != NULL) {
            RssiRecord *tofree = rssiTmp;
            rssiTmp = rssiTmp->next;
            free(tofree);
        }
        MacRecord *tmpToFree = tmp;
        tmp = tmp->next;
        free(tmpToFree->mac);
        free(tmpToFree);
    }
    tmp = NULL;
    macRecords = NULL;
}

void toString(char *data_str) {
    MacRecord *tmp = macRecords;
    strcpy(data_str, "");

    while(tmp != NULL) {
        RssiRecord *rssiTmp = tmp->rssiRecords;
        int i = 0;
        for (; i < 6; i++){
            sprintf(data_str, "%s%02x", data_str, tmp->mac[i]);
            if (i < 5)
                sprintf(data_str, "%s:", data_str);
        }
        sprintf(data_str, "%s %d\t", data_str, tmp->qtt);
        while (rssiTmp != NULL) {
            sprintf(data_str, "%s%d|%d|", data_str, rssiTmp->rssi, rssiTmp->timestamp);
            rssiTmp = rssiTmp->next;
        }
        sprintf(data_str, "%s\n", data_str );
        tmp = tmp->next;
    }
    freeAll();
}
