#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

/* Lenght of rssi circular buffer */
#define RSSI_CB_LENGTH 20
#define MAC_REC_LENGTH 4

/** Circular buffer structure */
typedef struct RssiRecord {
    uint8_t rssi;
    uint32_t timestamp;
    struct RssiRecord *next;
} RssiRecord;

/** Main structure */
typedef struct MacRecord {
    RssiRecord *rssiRecords;
    uint8_t *mac;
    uint8_t qtt;
    struct MacRecord *next;
} MacRecord;

/** Temp struct to return from search for mac */
struct MacRet {
    MacRecord *prev;
    MacRecord *curr;
};

/** Structure to hold info before insert */
typedef struct {
    uint8_t mac[6];
    RssiRecord *rssiRecord;
} WlanPacket;

/** Insert new record in circular buffer */
void vInsertRecord(WlanPacket wlanPacket);

/** Gets MacRecords structure into a string and clear MacRecords */
void toString(char *data_str);
