#include <espressif/esp_common.h>
#include <esp/uart.h>
#include <string.h>
#include <stdio.h>
#include <FreeRTOS.h>
#include "task.h"
#include "queue.h"
#include <semphr.h>
//#include "user_interface.h"
#include <stdint.h>
#include "circularbuffer.h"
#include "include/utils.h"

#include <paho_mqtt_c/MQTTESP8266.h>
#include <paho_mqtt_c/MQTTClient.h>

#define WIFI_SSID "BeRedi"
#define WIFI_PASS "okkam31370"

#define MQTT_HOST ("raspberry-ips.duckdns.org")
#define MQTT_PORT 1883

#define MQTT_USER NULL
#define MQTT_PASS NULL
#define PUB_MSG_LEN 3000
#define MQTT_TOPIC "indoor_positioning"

extern sdk_wifi_promiscuous_cb_t sdk_promiscuous_cb;
static TickType_t xLastWakeUpTime;

/** Queue used to store packets */
static QueueHandle_t xPacktQueue;

/** Semaphore to signal wifi mode - monitor/station */
static SemaphoreHandle_t wifiMode;

/** Get device mac to use as mqtt id */
const char * get_mac(void)
{
    /** Use MAC address for Station as unique ID */
    char *my_id = (char*) malloc (sizeof(char) * 18);
    int8_t i;
    uint8_t x;
    
    if (!sdk_wifi_get_macaddr(STATION_IF, (uint8_t *)my_id))
        return NULL;

    /** Translate mac to hex string */
    for (i = 5; i >= 0; --i)
    {
        x = my_id[i] & 0x0F;
        if (x > 9) x += 7;
        my_id[i * 2 + 1] = x + '0';
        x = my_id[i] >> 4;
        if (x > 9) x += 7;
        my_id[i * 2] = x + '0';

        my_id[i * 2 + i] = my_id[i * 2];
        my_id[i * 2 + i + 1] = my_id[i * 2 + 1];
        if (i > 0)
            my_id[i * 2 + i - 1] = ':';
    }
    
    my_id[17] = '\0';
    return (const char*)my_id;
}

/* Send data to mqtt broker */
uint8_t mqtt_send (char *client_id, mqtt_payload msg) {
    int ret = 0;
    struct mqtt_network network;
    mqtt_client_t client = mqtt_client_default;
    char mqtt_client_id[20];

    if (msg.len == 0) {
        printf("%s: invalid msg len\n", __func__);
        return -1;
    }

    uint8_t mqtt_buf[msg.len];
    uint8_t mqtt_readbuf[100];
    mqtt_packet_connect_data_t data = mqtt_packet_connect_data_initializer;

    mqtt_network_new( &network );
    memset(mqtt_client_id, 0, sizeof(mqtt_client_id));
    strcat(mqtt_client_id, client_id);

    printf("%s: Connecting to MQTT server %s ... ",__func__,
            MQTT_HOST);
    ret = mqtt_network_connect(&network, MQTT_HOST, MQTT_PORT);
    if( ret ){
        printf("error: %d\n", ret);
        return -1;
    }
    printf("OK\n");
    mqtt_client_new(&client, &network, 1000, mqtt_buf, 3000,
                    mqtt_readbuf, 100);

    data.willFlag       = 0;
    data.MQTTVersion    = 3;
    data.clientID.cstring   = mqtt_client_id;
    data.username.cstring   = MQTT_USER;
    data.password.cstring   = MQTT_PASS;
    data.keepAliveInterval  = 2;
    data.cleansession   = 1;

    printf("%s: Send MQTT connect ... ", __func__);
    ret = mqtt_connect(&client, &data);
    if(ret){
        printf("error while connecting: %d\n", ret);
        mqtt_network_disconnect(&network);
        return -1;
    }
    printf("OK\n");
    mqtt_message_t message;
    message.payload = msg.data;
    message.payloadlen = msg.len;
    message.dup = 0;
    message.qos = MQTT_QOS1;
    message.retained = 0;
    printf("%s: publishing message ... ", __func__);
    ret = mqtt_publish(&client, MQTT_TOPIC, &message);
    if (ret != MQTT_SUCCESS ){
        printf("error: %d\n", ret);
        return -1;
    }
    printf("OK\n");
    mqtt_network_disconnect(&network);
    return 0;
}

/* Print heap and stack free memory size */
void checkStackNHeap() {
    uint32_t free_heap = xPortGetFreeHeapSize();
    uint32_t free_stack = uxTaskGetStackHighWaterMark(NULL);
    printf("free heap %u, free stack %u\n",
         free_heap, free_stack * 4);
}

/* Hop between wlan channels */
void channelHop() {
    uint8_t new_channel = sdk_wifi_get_channel() % 11 + 1;
    sdk_wifi_set_channel(new_channel);
}

/* Timer interrrupt handler */
void frc1_interrupt_handler(void) {
    channelHop();
}

/* Callback called by the system when a packet
 * is captured by the interface
 */
void promisc_cb(uint8_t *buf, uint16_t len) {
    if (len == 12) {
        return;
    } else {
        TickType_t now = xTaskGetTickCount();
        WlanPacket *pkt = (WlanPacket*) malloc (sizeof(WlanPacket));
        RssiRecord *rssiRecord = (RssiRecord*) malloc (sizeof(RssiRecord));
        rssiRecord->timestamp = (uint32_t) ((now - xLastWakeUpTime));
        if (len == 128) {
            struct sniffer_buf2 *sniffer = (struct sniffer_buf2*) buf;
            int i = 0;
            for (; i < 6; i++) pkt->mac[i] = sniffer->buf[i + 10];
            rssiRecord->rssi = sniffer->rx_ctrl.rssi;
            free(sniffer);
        } else {
            struct sniffer_buf *sniffer = (struct sniffer_buf*) buf;
            int i = 0;
            for (; i < 6; i++) pkt->mac[i] = sniffer->buf[i + 10];
            rssiRecord->rssi = sniffer->rx_ctrl.rssi;
            free(sniffer);
        }
        pkt->rssiRecord = rssiRecord;
        if (xQueueSend(xPacktQueue, (void*)pkt, 10) == pdFALSE) {
            printf("%s: Failed to queue\n", __func__);
        }
        free (pkt);
    }
}

/*  Enables promiscuous mode on ESP8266 */
void enable_monitor_mode() {
    /* start the timer for channel hop */
    timer_set_frequency(FRC1, 20);
    timer_set_interrupts(FRC1, true);
    timer_set_run(FRC1, true);
    
    /* Configures the interface to station mode
     * with no auto connect 
     */
    sdk_wifi_station_set_auto_connect(0);
    sdk_wifi_station_disconnect();
    sdk_wifi_set_opmode(STATION_MODE);

    /* Registers the callback to capture packets 
     * and enables the promiscuous mode
     */
    sdk_wifi_promiscuous_enable(0);
    sdk_promiscuous_cb = &promisc_cb;
    sdk_wifi_promiscuous_enable(1);
}

/* Disables promiscuous mode */
void disable_monitor_mode() {
    timer_set_interrupts(FRC1, false);
    timer_set_run(FRC1, false);
    sdk_wifi_promiscuous_enable(0);
}

/* Enable esp as station and connects to network */
void enable_wifi_station() {
    struct sdk_station_config config = { .ssid = WIFI_SSID, .password =
        WIFI_PASS, };

    struct ip_info ap_ip;
    IP4_ADDR(&ap_ip.ip, 192, 168, 100, 103);
    IP4_ADDR(&ap_ip.gw, 192, 168, 100, 1);
    IP4_ADDR(&ap_ip.netmask, 255, 255, 255, 0);
    sdk_wifi_set_opmode(STATION_MODE);
    sdk_wifi_station_connect();
    sdk_wifi_set_ip_info(1, &ap_ip);
    sdk_wifi_station_set_config(&config);
}

/* Task to connect to network and send data */
void send_to_rpi (void *pvParameters) {
    /* Delay the application to monitor_task get semaphore first */
    vTaskDelay(3000 / portTICK_PERIOD_MS);
    uint8_t status = 0;
    bool wifi_alive = 0;
    uint8_t retries = 10;
    
    char str[PUB_MSG_LEN];
    char *mqtt_id = (char*) get_mac();
    printf("%s: starting %s\n", __func__, mqtt_id);
    while (1) {
        xSemaphoreTake(wifiMode, portMAX_DELAY);
        disable_monitor_mode();
        enable_wifi_station();
        printf("\n%s: Connecting to WiFi\n\r", __func__);
        retries = 15;
        wifi_alive = 0;
        status = 0;
        
        while ((status != STATION_GOT_IP) && (retries)) {
            status = sdk_wifi_station_get_connect_status();
            if (status == STATION_WRONG_PASSWORD) {
                printf("WiFi: wrong password\n\r");
                break;
            } else if (status == STATION_NO_AP_FOUND) {
                printf("WiFi: AP not found\n\r");
                break;
            } else if (status == STATION_CONNECT_FAIL) {
                printf("WiFi: connection failed\r\n");
                break;
            }
            vTaskDelay(500 / portTICK_PERIOD_MS);
            --retries;
        }

        while ((status = sdk_wifi_station_get_connect_status())
                == STATION_GOT_IP) {
            if (wifi_alive == 0) {
                printf("WiFi: Connected\n\r");
                wifi_alive = 1;
                break;
            }
            vTaskDelay(100 / portTICK_PERIOD_MS);
        }
        
        /** Get saved records in a string */
        toString(str);
        mqtt_payload payload;
        payload.data = str;
        payload.len = strlen(str);
        printf("%s: data length %d\n", __func__, strlen(str));
        checkStackNHeap();

        if (mqtt_send(mqtt_id, payload) == -1) 
            printf("%s: failed to send data\n", __func__);

        xSemaphoreGive(wifiMode);
        /* Let monitor_task task take the semaphore */
        taskYIELD();
    }
}

/* Task to enable monitor mode*/
void monitor_task(void *pvParameters) {
    /* Wait a little bit in order to be sure the 
     * init is done (no system_init_done_cb within rtos sdk)
     */
    printf("%s: starting\n", __func__);
    vTaskDelay(1000 / portTICK_PERIOD_MS );

    while(true) {
        xSemaphoreTake(wifiMode, portMAX_DELAY);
        printf("\n%s: setting monitor mode\n", __func__);
        enable_monitor_mode();
        vTaskDelay(5000/portTICK_PERIOD_MS);
        xSemaphoreGive(wifiMode);
        /* Let send_to_rpi task take the semaphore */
        taskYIELD();
    }
}

/* Task to read from the queue and save data */
void save_packts(void *pvParameters) {
    printf("%s: Starting\n", __func__ );
    WlanPacket wlanPkt;
    while(1) {
        if(xQueueReceive(xPacktQueue, &wlanPkt, portMAX_DELAY) != pdFALSE) {
            vInsertRecord(wlanPkt);
            free (wlanPkt.rssiRecord);
        } else {
            printf("%s: No msg\n", __func__);
        }
    }
}

void user_init(void) {
    /* Configures uart */
    uart_set_baud(0, 115200);
    printf("SDK version:%s\n", sdk_system_get_sdk_version());

    /* initialize timer to hop channels */
    timer_set_interrupts(FRC1, false);
    timer_set_run(FRC1, false);
    _xt_isr_attach(INUM_TIMER_FRC1, frc1_interrupt_handler);

    /** Starting queue and semaphores */
    xPacktQueue = xQueueCreate(100, sizeof(WlanPacket));
    xLastWakeUpTime = xTaskGetTickCount();
    vSemaphoreCreateBinary(wifiMode);
    
    /* Creates task to start monitoring */
    xTaskCreate(monitor_task, (const char *)"monitor_task", 1024, NULL, 2, NULL);
    xTaskCreate(save_packts, (const char *)"save_packts", 1024, &xPacktQueue, 2, NULL);
    xTaskCreate(send_to_rpi, (const char *)"send_to_rpi", 2048, NULL, 2, NULL);
}
