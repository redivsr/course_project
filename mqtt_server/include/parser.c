#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "parser.h"
#include "publisher.h"


char* source_macs[] = {"FF:AA:BB:CC:DD:EE", "FF:AA:BB:CC:DD:00", "FF:AA:BB:CC:DD:11" };

int getIndexOfMac(char* mac) {
    printf("MAC - %s\n", mac);
    for (int i=0; i<3; i++) {
        printf("MAC - %s %s %d\n", mac, source_macs[i],i);
        if (strcmp(mac, source_macs[i]) == 0) {
            return i;
        }
    }
    printf("Returned -1");
    return -1;
}

void parser(const char* entry) {

    char source[18];
    char device[18];
    char *measures;
    int num_measures;
    long size_measures = strlen(entry) - sizeof(source) - sizeof(device) - 2 + 1;
    char* tokens;
    measure* parsed_measures;
    int index_of_mac = -1;
    printf("%s %ld\n", entry, size_measures);

    measures = (char*)malloc (size_measures);

    sscanf(entry,"%s %s %d %s", source, device, &num_measures, measures);

    printf("Source %s\n", source);
    printf("Device %s\n", device);
    printf("Num Measures %d\n", num_measures);
    printf("Measures %s\n", measures);

    tokens = strtok(measures,"|");
    parsed_measures = (measure*) malloc(sizeof(measure) * num_measures);
    for (int i = 0; i < num_measures * 2; i++) {
        if ((i % 2) == 0) {
            memcpy(parsed_measures[i/2].device, device, 18);
            parsed_measures[i/2].rssi = atoi(tokens);                         
            
        } else {
            parsed_measures[i/2].timestamp = atoi(tokens);
        }
        printf( "%d %ld\n", parsed_measures[i/2].rssi, parsed_measures[i/2].timestamp );
        tokens = strtok(NULL, "|");
    }

    printf("finished tokenizing\n");

    index_of_mac = getIndexOfMac(source);
    if (index_of_mac != -1) {
        printf("--- Mac id %d\n", index_of_mac);
        last_measured_distances[index_of_mac] = parsed_measures[num_measures-1];

        Position *positions = (Position*) malloc(sizeof(Position)*num_measures);

        for (int i = 0; i < num_measures; i++) {
            Point pos = calculatePoint(parsed_measures[i], index_of_mac);
	    positions[i].x = pos.x;
            positions[i].y = pos.y;
            positions[i].timestamp = parsed_measures[i].timestamp;
        }

        publishJson(source, positions,  num_measures);
        free(positions);
    }

    printf("--- free pointers\n");
    free(measures);
    free(tokens);
    free(parsed_measures);
    printf("--- Finish parse\n");
}
