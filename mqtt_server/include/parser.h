//gcc -Wall -pedantic -o mosquitto_ mosquitto.c parser.c -lmosquitto
//gcc -Wall -pedantic  mosquitto.c parser.c -lmosquitto -o mosquito_2


struct last_measure {
    long timestamp;
    int rssi;
    char device[18];
    int distance;
};
typedef struct last_measure measure;


struct point 
{
    float x,y;
};
typedef struct point Point;

measure last_measured_distances[3];


Point trilateration(Point Point1, Point Point2, Point Point3, double r1, double r2, double r3);
void init_measures();
void initPoint(long x, long y, int pos);
Point calculatePoint(measure currentPoint, int currentMac);
void parser(const char* entry);
