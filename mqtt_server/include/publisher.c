#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "publisher.h"

const static char POS_X[4] = "posx";
const static char POS_Y[4] = "posy";
const static char TAB[4] = "    ";
const static int COORDINATE_LENGTH = 8;
const static int NODE_LENGTH = 60;
const static int TIMESTAMP_LENGTH = 12	;
const static int MAC_ADDRESS_LENGTH = 18;
const static char CURL[18] = "curl -X PUT -d '";

const static char URL[54] = "'https://spiconsumer-aa90e.firebaseio.com/macs.json'";

int buildNode(Position position, char**node) {
    int index=0;
    char local_node[NODE_LENGTH];
    char timestamp[TIMESTAMP_LENGTH];

    memset(local_node, 0, NODE_LENGTH);

    // timestamp
    local_node[index]='"'; index++;
    sprintf(timestamp, "%ld", position.timestamp);
    strncat(local_node, timestamp, TIMESTAMP_LENGTH);
    index+=TIMESTAMP_LENGTH;
    strncat(local_node, "\" : {\n", 6); index = index + 6;

    // posx
    strncat(local_node, "\"", 1); index++;
    strncat(local_node, POS_X, 4); index+=4;
    strncat(local_node, "\"", 1); index++;
    strncat(local_node, " : ", 3); index = index + 3;
    strncat(local_node, "\"", 1); index++;
    char x[COORDINATE_LENGTH]; sprintf(x, "%d", position.x);
    strncat(local_node, x, COORDINATE_LENGTH); index+=COORDINATE_LENGTH;
    strncat(local_node, "\",", 2); index = index + 2;
    strncat(local_node, "\n", 1); index++;
    
    // posy
    strncat(local_node, "\"", 1); index++;
    strncat(local_node, POS_Y, 4); index+=4;
    strncat(local_node, "\"", 1); index++;
    strncat(local_node, " : ", 3); index+=3;
    char y[COORDINATE_LENGTH]; sprintf(y, "%d", position.y);
    strncat(local_node, "\"", 1); index++;
    strncat(local_node, y, COORDINATE_LENGTH); index+=COORDINATE_LENGTH;
    strncat(local_node, "\"", 1); index++;

    // final parenthesis
    strncat(local_node, "\n", 1); index++;
    strncat(local_node, "}", 1); index++;
    *node = local_node;
   
//    printf("Node length %d %d\n", NODE_LENGTH, strlen(local_node));

    return strlen(local_node);
}

void publishJson(char* mac_address, Position positions[], int measures) {
    char* json;
    int index = 0;
    int size_of_json = (measures * NODE_LENGTH) + 2 + MAC_ADDRESS_LENGTH + 6 + (measures - 1) + 4 + 1;
    int command_size = strlen(CURL) + size_of_json + 2 + strlen(URL);
    json = (char*) malloc(sizeof(char) * size_of_json);
//    memset(&json, ' ', size_of_json);
    json[0]='{';
    json[1]='"';
    json[2]='\0';
    index+=2;
    strncat(json, mac_address, strlen(mac_address));
    index+= strlen(mac_address);
    strncat(json, "\" ", 2); index += 2;
    strncat(json, ": ", 2); index += 2;
    strncat(json, "{\n", 2); index += 2;
    char* node;

    for (int i = 0; i < measures; i++) {
        int size = buildNode(positions[i], &node);
        strncat(json, node, size); index+=strlen(node);
        if ((i != (measures - 1)) && measures > 1) {
            strncat(json, ",\n", 2); index+=2;
        }
    }
    strncat(json, "\n}\n}", 4); index+=4;

//    printf ("json size %d %d\n", size_of_json, strlen(json));


    char* command = (char*) malloc (command_size * sizeof(char));
    strncat(command, CURL, strlen(CURL));
    strncat(command, json, strlen(json));
    strncat(command, "' ", 2);
    strncat(command, URL, strlen(URL));

//    printf ("command size %d %d\n", command_size, strlen(command));

//    printf ("command \n %s\n", command);

    system(command);
    free(json);
    free(command);
}
