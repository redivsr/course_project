struct pos {
    long timestamp;
    int x;
    int y;
};

typedef struct pos Position;

void publishJson(char* mac_address, Position positions[], int measures);
