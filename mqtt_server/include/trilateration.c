#include <stdio.h>
#include <string.h>
#include <math.h>
//#include "trilateration.h"
#include "parser.h"

Point _points[3] = {{0.0,0.0} , {0.0,0.0}, {0.0,0.0}};

float norm (Point p) // get the norm of a vector
{
    return pow(pow(p.x,2)+pow(p.y,2),.5);
}

Point trilateration(Point Point1, Point Point2, Point Point3, double r1, double r2, double r3) {
    Point resultPose;
    //unit vector in a direction from Point1 to Point 2
    double p2p1Distance = pow(pow(Point2.x-Point1.x,2) + pow(Point2.y-   Point1.y,2),0.5);
    Point ex = {(Point2.x-Point1.x)/p2p1Distance, (Point2.y-Point1.y)/p2p1Distance};
    Point aux = {Point3.x-Point1.x,Point3.y-Point1.y};
    //signed magnitude of the x component
    double i = ex.x * aux.x + ex.y * aux.y;
    //the unit vector in the y direction. 
    Point aux2 = { Point3.x-Point1.x-i*ex.x, Point3.y-Point1.y-i*ex.y};
    Point ey = { aux2.x / norm (aux2), aux2.y / norm (aux2) };
    //the signed magnitude of the y component
    double j = ey.x * aux.x + ey.y * aux.y;
    //coordinates
    double x = (pow(r1,2) - pow(r2,2) + pow(p2p1Distance,2))/ (2 * p2p1Distance);
    double y = (pow(r1,2) - pow(r3,2) + pow(i,2) + pow(j,2))/(2*j) - i*x/j;
    //result coordinates
    double finalX = Point1.x+ x*ex.x + y*ey.x;
    double finalY = Point1.y+ x*ex.y + y*ey.y;
    resultPose.x = finalX;
    resultPose.y = finalY;
    return resultPose;
}

void init_measures() {
    for (int i=0; i < 3; i++) {
        last_measured_distances[i].timestamp = 0;
        last_measured_distances[i].rssi = 0;
        memset(last_measured_distances[i].device, ' ', 18);
    }

        last_measured_distances[1].timestamp = 1000;
        last_measured_distances[1].rssi = 100;
//        last_measured_distances[1].device = "FF:AA:BB:CC:DD:00";

        last_measured_distances[2].timestamp = 2000;
        last_measured_distances[2].rssi = 200;
//        last_measured_distances[2].device = "FF:AA:BB:CC:DD:11" ;


}


double getDistance(int rssi, int pos) {

// polynomial 3
//    double distance =  (-0.0003 * pow(rssi, 3)) + (0.0197 * pow(rssi, 2)) - (0.4666 * rssi) + 7.1621;
// polynomial 4
    double distance = (-0.00004 * pow(rssi, 4)) + (0.0036 * pow(rssi, 3)) - (0.1348 * pow(rssi,2)) + (2.0408 * rssi) - 6.4446;

    return distance;
}


Point calculatePosition(measure points[3]) {

    Point finalPose;
    finalPose = trilateration(_points[0],_points[1],_points[2],
                              getDistance(points[0].rssi, 0),
                              getDistance(points[1].rssi, 1),
                              getDistance(points[2].rssi, 2));

}

Point calculatePoint(measure currentPoint, int currentMac) {
    measure points[3];
    points[0] = last_measured_distances[0];
    points[1] = last_measured_distances[1];
    points[2] = last_measured_distances[2];
    points[currentMac] = currentPoint;

    return calculatePosition(points);

}

void initPoint(long x, long y, int pos) { 
    _points[pos].x = x;
    _points[pos].y = y;

    printf("%d %ld %ld\n", pos, x, y);
}

/*
int main(int argc, char* argv[]){
    Point finalPose;
    Point p1 = {0.0,0.0};
    Point p2 = {0.0,6.0};
    Point p3 = {4.0,5.0};
    double r1,r2,r3;
    r1 = 4;
    r2 = 3;
    r3 = 4;
    finalPose = trilateration(p1,p2,p3,r1,r2,r3);

    printf("finalPose.x %f finalPose.y %f", finalPose.x, finalPose.y);
}*/
