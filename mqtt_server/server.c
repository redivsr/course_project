#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <mosquitto.h>
//#include "include/trilateration.h"
#include "include/parser.h"

#define IP_MAX_LENGTH 15
#define MQTT_PORT 1883
#define MQTT_USERNAME "admin"
#define MQTT_PASSWORD "admin"
#define MQTT_TOPIC "indoor_position"

char MQTT_HOSTNAME[15];

void my_message_callback(struct mosquitto *mosq, void *obj, 
    const struct mosquitto_message *message) {
    printf ("Got message: %s\n", (char *)message->payload);
    parser((char *)message->payload);
}

void init_points(char** argv){
    long x=0, y=0;

    for(int i=1; i<4; i++) {
//        printf("%s %s\n", argv[2*i], argv[(2*i)+1]);
        x = atof(argv[2*i]);
        y = atof(argv[(2*i)+1]);
        initPoint(x, y, i-1);
    } 
}

/*
 * Start here
 */
int main (int argc, char **argv) {
 
    int ret = 0;
    struct mosquitto *mosq = NULL;

    memset(MQTT_HOSTNAME, ' ', IP_MAX_LENGTH);

    if (argc < 8) {
        printf("ERROR: Invalid entries");
        return -1;
    }

    int IP_length = (IP_MAX_LENGTH > strlen(argv[1]) ? strlen(argv[1]) : IP_MAX_LENGTH);

    memcpy(MQTT_HOSTNAME, argv[1], IP_length);
    printf("MQTT Server at %s\n", MQTT_HOSTNAME);

    init_measures();

    init_points(argv);

    mosquitto_lib_init();

    // Create a new Mosquitto runtime instance with a random client ID,
    //  and no application-specific callback data.  
    mosq = mosquitto_new (NULL, true, NULL);
    if (!mosq) {
        fprintf (stderr, "Can't initialize Mosquitto library\n");
        exit (-1);
    }

    // Establish a connection to the MQTT server. Do not use a keep-alive ping
    ret = mosquitto_connect (mosq, MQTT_HOSTNAME, MQTT_PORT, 0);
    if (ret) {
        fprintf (stderr, "Can't connect to Mosquitto server\n");
        exit (-1);
    }


    // Subscribe to the specified topic. Multiple topics can be
    //  subscribed, but only one is used in this simple example.
    //  Note that we don't specify what to do with the received
    //  messages at this point
    ret = mosquitto_subscribe(mosq, NULL, MQTT_TOPIC, 0);
    if (ret) {
        fprintf (stderr, "Can't publish to Mosquitto server\n");
        exit (-1);
    }
 
    // Specify the function to call when a new message is received
    mosquitto_message_callback_set (mosq, my_message_callback);

    // Wait for new messages
    mosquitto_loop_forever (mosq, -1, 1);

    mosquitto_destroy (mosq);
    mosquitto_lib_cleanup();


    return 0;

}

