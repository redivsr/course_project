#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/publisher.h"


int main (int argc, char **argv) {
    Position pos;
    pos.timestamp=1000000037;
    pos.x=100;
    pos.y=200;

    Position pos2;
    pos2.timestamp=1000000038;
    pos2.x=500;
    pos2.y=600;

    Position pos3;
    pos3.timestamp=100000039;
    pos3.x=500;
    pos3.y=600;

    Position pos4;
    pos4.timestamp=100000040;
    pos4.x=500;
    pos4.y=600;

    Position pos1[4];
    pos1[0] = pos;
    pos1[1] = pos2;
    pos1[2] = pos3;
    pos1[3] = pos4;

    publishJson("06-00-00-00-00-00", pos1, 4);
    return 0;
}
